package regression

import (
	"gonum.org/v1/plot/plotter"
	"github.com/gonum/stat/distuv"
	"image/color"
	"gonum.org/v1/plot/vg"
	"math/rand"
	"gonum.org/v1/plot"
	"github.com/kniren/gota/dataframe"
	"fmt"
	"log"
	"os"
)

func (l *LNR) View1(normalizedRecord map[string][]float64) {

	rand.Seed(int64(0))
	v := make(plotter.Values, 10000)
	for i := range v {
		v[i] = rand.NormFloat64()
	}


	v = make(plotter.Values, len(normalizedRecord["km"]))
	for i := range v {
		v[i] = normalizedRecord["km"][i]
	}

	fmt.Println(v)

	h, err := plotter.NewHist(v, 23)
	if err != nil {
		panic(err)
	}

	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	//h.Normalize(1)
	p.Add(h)

	norm := plotter.NewFunction(distuv.UnitNormal.Prob)
	norm.Color = color.RGBA{R: 255, A: 255}
	norm.Width = vg.Points(2)
	p.Add(norm)

	// Save the plot to a PNG file.
	if err := p.Save(4*vg.Inch, 4*vg.Inch, "hist.png"); err != nil {
		panic(err)
	}

}

func (l *LNR) View(normalizedRecord map[string][]float64) {
	f, err := os.Open("data_frame/data.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	df := dataframe.ReadCSV(f)

	pts := make(plotter.XYs, df.Nrow())

	yVals := df.Col("price").Float()
	for i, floatVal := range df.Col("km").Float() {
		pts[i].X = floatVal
		pts[i].Y = yVals[i]
	}

	p, err := plot.New()
	if err != nil {
		log.Fatal(err)
	}


	p.X.Label.Text = "km"
	p.Y.Label.Text = "price"

	p.Add(plotter.NewGrid())

	s, err := plotter.NewScatter(pts)
	if err != nil {
		log.Fatal(err)
	}
	s.GlyphStyle.Radius = vg.Points(2)
	s.GlyphStyle.Color = color.RGBA{R: 0, G: 0, B: 255, A: 255}

	// Add the line plot points for the predictions.
	//l, e := plotter.NewLine(ptsPred)
	//if e != nil {
	//	log.Fatal(err)
	//}
	//l.LineStyle.Width = vg.Points(0.5)
	//l.LineStyle.Dashes = []vg.Length{vg.Points(2), vg.Points(2)}
	//l.LineStyle.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	//// Save the plot to a PNG file.
	p.Add(s)
	if err := p.Save(15*vg.Centimeter, 15*vg.Centimeter, "./graphs/first_regression.png"); err != nil {
		log.Fatal(err)
	}


}