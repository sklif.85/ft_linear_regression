package regression

import (
	"strconv"
	"fmt"
)

func (l *LNR) ReShaper(records [][]string) map[string][]float64 {

	newRecords := make(map[string][]float64, len(l.Headers))

	for _, record := range records {
		for i, name := range l.Headers {
			f, e := strconv.ParseFloat(record[i], 64)
			if e != nil {
				fmt.Println(e)
			}
			newRecords[name] = append(newRecords[name], f)
		}
	}

	return newRecords
}