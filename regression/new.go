package regression

type Model interface {
	New() *Model
	Reader()
	Normalizer(map[string][]float64)
	View(map[string][]float64)
}

type LNR struct {
	File    string
	Column  int
	Headers []string
}

func New(filePath string, countColumn int) *LNR {
	return &LNR{
		File:   filePath,
		Column: countColumn,
	}
}
