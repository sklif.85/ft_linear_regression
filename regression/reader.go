package regression

import (
	"os"
	"encoding/csv"
	"fmt"
	"io"
	"log"
)

func (l *LNR) Reader() [][]string {
	file, err := os.Open(l.File)
	if err != nil {
		panic(err)
	}

	defer closer(file)

	data := csv.NewReader(file)
	data.FieldsPerRecord = l.Column

	records, err := data.ReadAll()
	if err != nil {
		fmt.Println(err)
	}

	l.Headers = records[0]

	return records[1:]
}

func closer(w io.Closer) {
	err := w.Close()
	if err != nil {
		log.Fatal(err)
	}
}
