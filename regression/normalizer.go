package regression

import (
	"sort"
)

func (l *LNR) Normaliser(records map[string][]float64) map[string][]float64 {

	normRecord := map[string][]float64{}
	for k, v := range records {
		normRecord[k] = doNormalise(v)
	}

	return normRecord
}


//zi = xi−min(x) / max(x)−min(x)
func doNormalise(record []float64) []float64 {

	normRecord := make([]float64, len(record))

	sort.Float64s(record)

	minVal := record[0]
	maxVal := record[len(record)-1]

	//mean := (func() float64 {
	//	summ := 0.0
	//	for _, n := range record {
	//		summ += n
	//	}
	//	return summ
	//}()) / float64(len(record))

	for i, val := range record {
		normRecord[i] = (val - minVal) / (maxVal - minVal)
	}

	return normRecord
}
