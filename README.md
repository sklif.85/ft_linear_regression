# ft_linear_regression

An introduction to machine learning with Golang

# Objective
The aim of this project is to introduce you to the basic concept behind machine learning.
For this project, you will have to create a program that predicts the price of a car
by using a linear function train with a gradient descent algorithm.
We will work on a precise example for the project, but once you’re done you will be
able to use the algorithm with any other dataset.