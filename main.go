package main

import (
	"github.com/ft_linear_regression/regression"
	"fmt"
)

const filePath = "data_frame/data.csv"

func main() {

	lnr := regression.New(filePath, 2)

	records := lnr.Reader()

	reshapedRecords := lnr.ReShaper(records)

	normalizedRecord := lnr.Normaliser(reshapedRecords)

	fmt.Println(records)
	fmt.Println(reshapedRecords)
	fmt.Println(normalizedRecord)

	lnr.View(normalizedRecord)

}
